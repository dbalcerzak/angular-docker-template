# AngularTemplate

This simple project is based on `ng new angular-project-name`. The idea is to store the simplest (as far as I know) angular base project with `Dockerfile`, `nginx.conf` configuration file and `.gitlab-ci.yml` CI/CD gitlab pipeline. I wanted to store base project for my future projects  because I always forget how to write those three files from scratch :P and why not to share with other Developers? Also `Dockerfile`, `nginx.conf` and `.gitlab-ci.yml` files are as simple as I could. I was always pissed off when I search those files over Internet and I found some strange and unecessary synthax.

Docker image is prepared to host angular app on nginx server. You can build image by running this command in project root:
`docker build -t angular-template . `. After build you can run it in a container: `docker run -d -p 80:80 angular-template` and that's it! It will be available under address [http://localhost]

If you want to run project locally without a container remember to `npm i` and then `npm run start`.

There is deploy example to GitLab pages on `.gitlab-ci.yml`. It is not obvious to use it with Angular app. It is necessary to build it with base path as part of GItLab URL which is mapped project name -> e.g.: this project is under URL: [https://gitlab.com/dbalcerzak/angular-docker-template] which is `https://gitlab.com/<username>/<project>` so GitLab Pages URL is [https://dbalcerzak.gitlab.io/angular-docker-template]. This mechanism is described in documentation: [https://docs.gitlab.com/ee/tutorials/hugo/#deploy-and-view-your-hugo-site].

Enjoy!

#angular 
#docker 
#gitlab 
#ci/cd 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.6.
