FROM node:20.5.0-alpine3.18 as builder

WORKDIR /app
COPY ./*.json ./*.js ./

RUN npm ci

COPY . .

RUN npm run build -- --output-path=/deploy/

###############################################

FROM nginx:stable-alpine3.17-slim as hosting

COPY ./nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /deploy /usr/share/nginx/html